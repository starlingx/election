Juanita Balaraj Candidate for StarlingX Docs Technical Lead role
Affiliation: Wind River Systems.

Hello StarlingX community, 
I would like to continue to volunteer as the TL for the StarlingX docs project.
This has been an interesting phase in my role as Technical lead for the past 2+
years. We have enhanced the quality and content of the StarlingX docs by providing
more value to the community and increasing participation from the community.

My goal is to continue improving and enhancing the StarlingX documentation content
and quality and increasing more pariticipation from the community.

Thank you for your kind consideration and I look forward to serving the StarlingX
Docs project.

Juanita Balaraj