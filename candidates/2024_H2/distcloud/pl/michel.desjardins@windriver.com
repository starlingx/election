Michel has been a member of the Wind River team for the last year and is the Manager of  the Distributed Cloud team. Michel has been leading the deliveries of the features for stx9.0 and stx10.0.
Michel has previous opensource experience in working on ONF projects including SEBA/VOLTHA.
Michel is looking forward for further contributions to StarlingX. Thanks
