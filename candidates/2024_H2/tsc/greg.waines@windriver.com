StarlingX continues to be an exciting and growing project to which I have
been lucky enough to contribute to from its launch in early 2018.  Over
the past 6 years I have provided technical guidance to several of
StarlingX's subproject teams, represented StarlingX in OpenStack's Edge
Computing Group, and participated directly in the StarlingX Documentation
subproject.

I have a wide breadth of knowledge on StarlingX's system architecture and
design, and as such have been a frequent speaker at OpenStack Summits on
StarlingX topics.

As part of the TSC, I am interested in:
- continuing to support the open development model within StarlingX,
- helping to define the upcoming StarlingX Releases to ensure success,
   * meeting the functional needs of our users, and
   * delivering quality releases.
- helping to increase and diversify the contributor base of StarlingX,
- helping to increase and diversify the user base of StarlingX,
- supporting and helping in the definition of new areas of growth for
  StarlingX.

Continue to make StarlingX into a diverse and thriving open source project.
