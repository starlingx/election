My name is Thales Elero Cervi and I am nominating myself for election
to the StarlingX Openstack-Distro PL role.


I'm working with the stx-openstack application for two years and was
elected for the TL role last election (2022 H2). I am nominating myself for 
both roles this time (2024 H2), while we prepare a new TL for this project
as part of the next tenure.
