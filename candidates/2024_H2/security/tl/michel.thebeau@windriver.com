Michel Thebeau: Candidate for Security Technical Lead role
Affiliation: Wind River Systems

I've been an active member of the StarlingX security team for several
years.  I provide technical responses for the status of CVEs and fixes
related to the Starlingx project.  I review the Starlingx summary of
Vuls scan reports, and the Openstack Security Advisories and Notes.  I
contribute to Starlingx security related features and code reviews
including: oidc, vault, portieris.
