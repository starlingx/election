Ghada Khalil: Candidate for StarlingX Release Technical Lead role
Affiliation: Wind River Systems

I've held the position of PL/TL for the StarlingX release project since
the beginning of StarlingX. I've helped plan, track, and deliver StarlingX
releases over the years. I enjoy the role and would like to continue in it.
