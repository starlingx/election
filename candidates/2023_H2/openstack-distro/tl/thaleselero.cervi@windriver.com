My name is Thales Elero Cervi and I am nominating myself for election
to the StarlingX Openstack-Distro TL role.


I'm working with the stx-openstack application for two years and was
elected for the TL role last election (2022 H2). It has been a pleasure
to be part of this community since day zero and I would like to continue
leading the Openstack Distro team development and test efforts for our
upcoming releases.

My goal for the project is to ensure that the stx-openstack application is
stable, reliable, updated with the latest OpenStack releases and compatible
with the latest versions of StarlingX. Our short-term goal is to have a
stable stx-openstack application, delivering OpenStack 2023.1 (Antelope)
services for stx/9.0 release and later work on having it already updated to
OpenStack 2023.2 (Bobcat) for stx/10.0.
I also care for the testing activities to ensure software quality and that
upgrade processes are covered properly.
