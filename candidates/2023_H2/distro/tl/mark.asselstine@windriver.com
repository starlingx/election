Mark Asselstine candidate for StarlingX Distro Technical Lead role

Affiliation: Wind River Systems

Having been in the Distro Technical Lead for several years now I hope
to continue in this role as we refine our use of Debian and extend to
the ARM architecture.
	      
Thanks,
Mark
