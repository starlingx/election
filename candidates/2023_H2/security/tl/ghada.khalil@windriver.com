Ghada Khalil: Candidate for Security Technical Lead role
Affiliation: Wind River Systems

I've been an active member of the StarlingX security team for several
years. I've primed the delivery of multiple features/enhancements in the
security domain. I also participate in the monthly CVE scanning process.
