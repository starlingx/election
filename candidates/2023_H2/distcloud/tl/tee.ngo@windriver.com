Candidate for StarlingX Distributed Cloud Technical Lead Role

I have contributed to the StarlingX project from the inception.  In particular,
I have developed the architecture updates, feature specifications and code in
several key areas including ansible playbooks, config and distributed cloud.

I developed the seed code for the ansible playbooks repo and the application
framework of config repo. In the past StarlingX releases, I had driven the
delivery of high impact DC capabilities such as scalability improvements
(500% increase), increase in parallel operations, subcloud prestaging,
transition of DC from CentOS to Debian.

Currently, I am both participating and driving the delivery of new and
transformational DC capabilities for StarlingX 9.0.

As technical lead for distributed cloud and core reviewers of several
StarlingX projects, I would focus on:
* Ensuring high quality by providing guidance on specs, assistive feedback on
  code reviews that encourage improvements and foster knowledge sharing.
* Growing contributors to distributed cloud project by enabling ease of
  contribution and soliciting developers and users’ feedback.
* Evolving the distributed cloud features so that its scalability, versatility,
  and ease of operations will enable many more use cases thus
  increasing StarlingX adoption.
* Providing input on new capabilities in other StarlingX projects to ensure
  their successful integration and operations in distributed cloud environment.

Thank you for your consideration for my candidacy as the technical lead
for the StarlingX distributed cloud project.

Tee Ngo
