My name is Scott Kamp. I nominate myself to the election for StarlingX
TSC memberTechnical Steering Committee. I am a current TSC member. I have 
been using and advocating StarlingX for years and have been actively 
working on technical additions to improve StarlinX platform overall.

I have been an advocate of StarlingX and have worked on many aspects of 
improving functionalities. wireguard integration, encrypted communications 
between pod and clusters, improving networking csi with the addition of 
Anthea, a calico alternative, and currently engadged in restoring rook-ceph
toa function working state, also community iso builds and patches. Advocating
for StarlingX adoption in many arenas, including Telecomm, IOT, Wireless ISP.
Making it easier for community members to collaborate and to continuously grow
the community, remove the barriers to user adoption.

Thank you for your consideration for me to continue serving StarlingX project.

Scott V. Kamp

