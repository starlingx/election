My name is Steve Geary, and I am self-nominating for an open TSC (Technical Steering Committee) role in the StarlingX community.

I have been a citizen of the StarlingX community for nearly three years. During that time, I actively worked to grow the community, increase community participation diversity, and improve StarlingX Open Source practices.

I have 20+ years of open source experience, which includes serving on the Board of Directors groups of multiple projects, assisting with the creation of a Linux distribution, and developing and executing on an open source strategy for a Forbes Fortune 50 company. I would like to contribute my experience and expertise to be a valuable member of the StarlingX TSC.

StarlingX is now 5 years old and is established with regular releases, active working groups, and commercial deployments. The focus on the technology and solution has been core to StarlingX success. My observation is that success has fostered interest from individuals and companies across the planet that the community is not currently able to respond appropriately to.

As a member of the TSC, my priority would be to resolve the challenges StarlingX faces today. Challenges include:

* Communication with both StarlingX established contributors and newcomers.
* Onboarding new users
* Onboarding new contributors
* Onboarding infrastructure contributions
* Enabling the burden share of shared needs (build, test, triage, …)

I thank you for considering my nomination to serve you and this community.

Sincerely,
Steve Geary
