My name is Wang Hao and I am nominating myself for election to the StarlingX
Technical Steering Committee. I have been a contributor in OpenStack since
2013 and now is the PTL of Zaqar project.

I'm an newbie in here but I have started to learn those projects and tried 
my best to do contribution for several months. StarlingX is a very
important open source project in the Edge area and I firmly believe we
can do much amazing works in here. I, also my colleagueswill involve
StarlingX more in future and bring this project into more use-cases in
China market.

I also would like to introduce StarligX to more Chinese developers and
encourage more contributors to join us to build this project more powerful.
This is one of the most important guarantees for healthy growth of StarlingX
I think.

It's a fantastic experience working with this team and I would be
pleased to serve as TSC to push on StarlingX better in 2019.

Thank you all
