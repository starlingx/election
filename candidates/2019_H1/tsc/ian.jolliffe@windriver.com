Ian Jolliffe: Candidate for StarlingX TSC Election
Affiliation: Wind River Systems

StarlingX is an exciting and growing project with which I have had the honor and
privilege to contribute to from its launch.   I am a founding member of the TSC
and over the past year I have helped facilitate the TSC, welcome new members and
help guide the project from a technical perspective.  I am a frequent speaker at
industry events on the values of StarlingX and share my passion for edge
computing widely.  I see edge as having the ability to transform people's lives
through transformation of technology used in health care and transportation -
StarlingX is a big part of making that happen.

I have contributed to OPNFV and other open source projects over the years.  I
have a deep technical knowledge of distributed computing and optimization of
edge focused solutions. I have helped out in the Edge Computing talk selection
committee for the last summit and will have volunteered to do so again for
Shanghai.

I was honored and humbled to receive a community award at the last summit, for
my contributions to the StarlingX project - [0]

On the TSC I would have a few areas of focus:

Continue to advocate for the 4 Open model of development.  Help the project pass
the graduation criteria from the pilot phase as defined by the OpenStack
foundation.  My goal is to champion and contribute to increasing test focus and
culture of the project around test. This area is so important to the health of
any open source project (well any project actually).  This will help new
developers contribute more easily and the project to grow the contribution base
and speed of development.  We had a good discussion at the PTG in Denver a few
weeks ago.    I plan on working with the community to find ways  we can better
leverage Zuul and other projects to accelerate the project and increase quality
of the code.

I am focused on growing the community, maintaining the technical relevance of
the project and continuing StarlingX's journey to a diverse and thriving open
source project.  I value the contributions of the OpenStack Foundation and their
guidance is something that has helped the project on our journey to graduate the
pilot phase.

Thank you for your consideration for me to continue as a contributor to the TSC
and the Starling X project overall.

[0] https://superuser.openstack.org/articles/open-infrastructure-community-contributor-awards-denver-summit-edition/
