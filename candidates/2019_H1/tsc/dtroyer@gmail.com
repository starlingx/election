My name is Dean Troyer and I am nominating myself for election to the StarlingX
Technical Steering Committee.  I have been a long-time OpenStack contributor,
I am the current PTL for OpenStackClient and a past member of the OpenStack
Technical Committee.

I have been working on StarlingX since the beginning of the open-sourcing
effort in January 2018, preparing the large codebase to be released and
analyzing and reconciling the changes made to upstream OpenStack.  Since
the initial public release in May 2018 I have worked on establishing the
community infrastructure, and adapting processes and tools from OpenStack
to fit the needs of StarlingX.

I would like to see StarlingX evolve to unlock more of the potential in the
additional 'flock' services and make them available outside the narrow
environment of a fully-integrated distro.  This not only increases the value
of these services but makes it easier to evaluate and adopt them without
requiring the entire stack.

I also see value in separating some of the components of StarlingX as an aid
to engaging new contributors.  The current requirements for building the
system discourage all but the most motivated contributions.  One of the
priorities for the TSC is to grow the community of both contributors and
deployers/operators.  This is one of the specific things that StarlingX is
being evaluated on for the final confirmation as a top-level project in the
OpenStack Foundation.

I would be honored to continue serving the StarlingX community on the Technical
Steering Committee as we head into our second year as a community-driven project.

Thank you
dt
