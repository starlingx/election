My name is Bart Wensley and I am nominating myself for the technical lead
position in the distcloud (Distributed Cloud) project.

I have been contributing to the StarlingX project from day one and to the
pre-cursor product at Wind River for several years before StarlingX was
launched. I was a key contributor to the original architecture/design of
the distcloud project and was the author of a significant amount of the code
in this project, including the dcmanager and the patch orchestration
components. I am a core reviewer for the repos in this project and I have
reviewed the majority of the commits in this project to help maintain a high
level of quality.

As the technical lead for the distcloud project I would focus on:
* Maintaining the high level of quality. I will do this by continuing to
  review all feature specifications in this project and provide guidance.
  I will also continue reviewing the majority of the commits going into
  distcloud as a core reviewer.
* Growing the contributors to distcloud. I will do this by providing  clear
  and helpful guidance to contributors during both the specification and
  implementation stages of development, making it as easy as possible for new
  contributors to get started.
* Improving the technical relevance of distcloud by promoting new features
  that increase the value of distcloud for both existing users and future
  users of StarlingX.

Thank you for your consideration for me to serve as the technical lead for
the distcloud project in StarlingX.

Bart Wensley
