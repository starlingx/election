Yong Hu: Candidate for the openStack-distro project lead role
Affiliation: Intel

My name is Yong Hu and I am nominating myself for the project lead in the openstack-distro project.
I have been working on the StarlingX project from day one at Intel and contributing to StarlingX
releases R0, R1 and R2.

In the past 15 months, as an engineer, I made contributions on enabling features, upgrading
components, resolving or analyzing bugs.
As a community memember, I think it's important to continously invest energy to promote StarlingX
as a solution to the business partners. So accordingly, I've together with team, made several demos
showed in Openstack summits, KubeConf and a few other conferences, such as, "StarlingX and EdgeX for
IOT AI workloads", "StarlingX as edge server for Industrial PLC", "StarlingX as Infra for FlexRAN",
and so on.

In addition, In July and August 2019, I was, on behalf of the current project lead (Bruce Jones),
driving the efforts in this project. The major activities covered project meetings, bug fixing
follow-up and coordination with other project teams.
Further, right after 2.0 release, in Sept we had a successful meetup in China Beijing,  with 80+
other members or customers. In this meetup, I was one of organizers and also presented a session
to all the audiences about StarlingX 2.0 highlights.

As the project lead for the openstack-distro project I would focus on:
* keeping the close connection with OpenStack upstream, by working together with TL and project
members who are also contributors in OpenStack core projects, so that StarlingX can efficiently
keep up with OpenStack, particularly follow up OpenStack upgrade and the evolving key features.

* Maintaining the high level of quality in the integration of OpenStack services.

* Growing the contributors into openstack-distro project. Any kinds of contributions will be welcome
in this project, and we can make StarlingX a better solution by but not limited to: improving documents,
fixing issues, enabling features, reporting issues, answering questions, and so on.

Thank you for your consideration for me to serve as the project lead for the openstack-distro project
in StarlingX.

Yong Hu


