Brent Rowsell: Candidate for StarlingX TSC Election
Affiliation: Wind River

I have had the honor and privilege of contributing to the StarlingX project
since January 2018. Edge computing has the ability to transform people's lives
and StarlingX plays a role in making this happen.

As a founding member of the StarlingX TSC I have facilitated the definition
of the initial seed code architecture, the evolution of the project to a cloud
native platform supporting openstack and containers as first class citizens,
openstack patch elimination, distributed edge computing and overall feature
definition for our releases. I have been a presenter at industry events promoting
the values of StarlingX.

My goals moving forward would be to champion and contribute to:

Growing and extending our distributed edge capabilities, adoption and creation of
new cloud technologies to address edge use cases evolving beyond the current
set of "flock" services.

Removing barriers to user adoption by continue to make StarlingX easier to consume,
deploy and operate.

Simplifying the contributor experience by streamlining our processes and development
environment.

Growing the diversity and number of contributors to the project by facilitating
opportunities for people to get involved in new project initiatives and/or bringing
their own.

Thank you for your consideration for me to continue as a contributor to the TSC
and the Starling X project overall as we continue our journey.


Brent Rowsell
