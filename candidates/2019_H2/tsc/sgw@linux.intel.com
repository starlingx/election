Saul Wold: Candidate for StarlingX TSC Election
Affiliation: Intel

I am one of the founding members of the StarlingX TSC and been involved with
the project since June of 2018. My focus area has been working with CentOS
and setting a direction for moving forward with support for  additional OSes
such as openSUSE.

The StarlingX project is continuing to grow and we need to encourage the
community to work in the open and follow the 4 Opens model of development.
As we gain new community members, it's important to get involvement 
from them to the various sub-projects, including flock services, Core OS,
test, build infrastructure and documentation.

My background has been more in the Linux Operating System Build, Install and
Deployment, coming from working with other open source projects such as the
Yocto Projet (Embedded Linux). Currently, I am working closely on extending
the the tools and testing to support multiple OSes and trying to improve the
initial developer experience.

I appreciate your time and consideration of me to continue serving on the
StarlingX Project Technical Steering Committee.

Thanks
   Sau!

Saul Wold
