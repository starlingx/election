Matt Peters: Candidate for StarlingX Networking Technical Lead role
Affiliation: Wind River Systems

I bring over 15 years of expertise in networking and product development.

I have been contributing to the StarlingX project since its inception,
providing both technical guidance and planning input.  I take an active
role in reviewing contributions to StarlingX and providing meaningful and
helpful input during the bi-weekly networking project community calls.

As the technical lead for the networking project I would focus on:
* Maintaining the high level of quality in the networking domain, with
  constructive and supportive guidance to existing and new contributors.
* Present a renewed focus on expanding the networking capabilities of the
  container platform by exploring and integrating new networking technologies.
* Place an emphasis on growing the project, making it an inviting place
  for new members and contributors.

Thanks for your consideration of the StarlingX Networking Technical Lead role.

Matt Peters
