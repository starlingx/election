My name is Bart Wensley and I am nominating myself for the technical lead
position in the flock-services project.

I have been contributing to the StarlingX project from day one and to the
pre-cursor product at Wind River for several years before StarlingX was
launched. Over the years I have contributed to the architecture/design of all
the services that make up the new flock-services project. I have contributed
code to most of these services and I have reviewed many of the commits in
these projects to help maintain a high level of quality.

As the technical lead for the flock-services project I would focus on:
* Maintaining the high level of quality in the flock services. I will do this
  by continuing to review all feature specifications in this project and
  provide guidance. I will also continue reviewing the majority of the
  commits going into the flock services as a core reviewer.
* Growing the contributors to the flock services. I will do this by providing
  clear and helpful guidance to contributors during both the specification and
  implementation stages of development, making it as easy as possible for new
  contributors to get started.
* Improving the technical relevance of the flock services by promoting new
  features that increase the value of the flock services for both existing
  users and future users of StarlingX.

Thank you for your consideration for me to serve as the technical lead for
the flock-services project in StarlingX.

Bart Wensley
