Cindy Xie: Candidate for the openStack-distro project lead role
Affiliation: Intel

My name is Cindy Xie and I am nominating myself for the project lead position in the flock-services project.

I have been contributing to the StarlingX project from day one and even since StarlingX seeding release from Intel.
In the past 15 months I am the PL for non-OpenStack-distro project, and drive related activities in this project, including the project tasks coordination, project planning and facilitating project executions to secure all commitments achieved for StarlingX milestones.

Moving forward I would like to shift my project lead role from non-openstack-distro to flock-services project, because I believe, on such a crucial foundation as flock-services is, my years of project experience, coordination skills and influence capabilities may bring more benefits and energy to StarlingX, as an open-source project and an edge solution.

As the project lead for the flock-services project I would focus on:
* Maintaining the high level of quality in the flock services, and securing StarlingX releases delivered on schedule.
* Taking advantage of TL's tech insights and expertise, making flock-services evolve efficiently to meet new requirements.
* Foster the open source inclusive culture so that we can attract broad base of contributors from community.

Thank you for your consideration for me to serve as the project lead for the flock-services project in StarlingX.

Cindy Xie

