Saul Wold: Candidate for StarlingX MultiOS Technical Lead
Affiliation: Intel

As the current Technical Lead (TL) for the MultiOS sub-project, I would like
to continue working for the community to developing and improving the flock
services to enabling migrating them to different Linux OSes as individual
services.

I bring a background of development in Open Source and multiple Linux
Operating Systems and build tools.
tools.

I appreciate your time and consideration of me to continue serving on
the StarlingX Project MultiOS team as Technical Lead.

Thanks
   Sau!

Saul Wold
