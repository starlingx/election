Marcela Rosales: Candidate for MultiOS Project Lead role
Affiliation: Intel

My name is Marcela Rosales and I nominate myself for the MultiOS Project Lead
role.

I started contributing to StarlingX project on June 2018 as part of the Build
team. There I focused on improving mirror download tools and user experience.
In January 2019 I started working on MultiOS effort by packaging flock services
for Debian Operating System (OS) and later for openSUSE OS. I have been driving
MultiOS weekly meeting for a couple of months now.

Right now the MultiOS project continues with the enabling of StarlingX flock
services on an openSUSE system and its appropriate configuration.

I appreciate your consideration of me to serve as Project Lead for StarlingX
MultiOS project.

Thanks.

Marcela
