Saul Wold: Candidate for StarlingX Build Technical Lead
Affiliation: Intel

As the current Technical Lead (TL) for the Build sub-project, I would like
to continue working for the community to improve the developer experience
and encourage adoption of StarlingX by new developers and deployers.

I bring a background of development in Open Source projects and OS build
tools.

I appreciate your time and consideration of me to continue serving on
the StarlingX Project Build team as Technical Lead.

Thanks
   Sau!

Saul Wold
