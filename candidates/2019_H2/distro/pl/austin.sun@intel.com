Austin Sun: Candidate for StarlingX Distro Project Lead role
Affiliation: Intel
I have been contributing to the StarlingX project since StarlingX Day one.

As the project lead for the Distro project I would focus on:
* Growing and extending Distro Project capabilities, adoption and creation of
new technologies to serve StarlingX Project.

* Simplifying the contributor experience by improving processes and development
environment and growing the diversity and number of contributors to the project by
standardizing document and wiki.

* Reinforcing interaction between Distro and other StarlingX projects by Studying
  StarlingX new feature, design and requirement.

Thank you

Austin Sun
