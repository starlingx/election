Bob Church: Candidate for StarlingX Containers Technical Lead role
Affiliation: Wind River Systems

As a current StarlingX contributor and core reviewer for numerous StarlingX
repositories, I would like to serve as TL for the Containers project.

I bring over five years of combined product knowledge with the Wind River seed
code and derived StarlingX project code. Historically, I have contributed code
across most areas of the project and am well versed in debugging system issues.

In the past, my focus has been in area of system storage and their interactions
with Openstack services (cinder, glance, nova, etc). With the re-architecture to
making StarlingX a Kubernetes platform, I helped lead the containerization
effort of Openstack services via the integration of the openstack-helm project.

As the TL for the containers project, I would work closely with the PL to help
drive the implementation of new TSC approved features and improvements around
existing functionality. I would like to work with the community to achieve these
goals.

Thanks for your consideration.

Bob Church
