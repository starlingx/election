Victor Rodriguez: Candidate for Security Technical Lead role
Affiliation: Linux Operating System Engineer

My name is Victor Rodriguez and I am nominating myself for the technical lead
position in the security project.

I have been contributing to the StarlingX project from a year ago in security
in topics such as:

* Enablement of the Vuls Scan tool to get the list of CVEs affected
* Suggest compiler security flags
* Analysis of upstream patches for CVE fixes


As the technical lead for the security project I would focus on:

* Maintaining the security as a high priority for the StarlingX project. I will
do this by maintaining the security scan updated and look for other new tools
that help to detect the security bugs earlier.

* Scan latest security news for upstream bugs. Sometimes the CVE scan tools do
not catch all the security bugs. Is important to keep a regular scan on the
tech news for high severity security bugs

* Provided a digested patch ready to be merge for the development team. The CVE
scan tool could be automated to the level of providing the patch that fix the
issue if the patch is upstream at the moment.

* Work with upstream if necessary to get a fix to the security issue. The
security of the StarlingX users is the top priority for me, spending time with
upstream to fix the security issue is not a problem and I can do it with joy.

Thank you for your consideration for me to serve as the technical lead for
the security project in StarlingX.

Victor Rodriguez

