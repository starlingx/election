My name is Thales Elero Cervi. I'm nominating myself to the StarlingX Technical
Steering Committee election, after being part of the stx-openstack application
project for three years, two of which I took on the role of the stx-openstack
TL (since 2022 H2 election).

It has been a pleasure to be part of this community since day zero and I would
like to extend my contributions being part of the TSC.
I do have a good knowledge on the application framework, due to my working
experience with stx-openstack, but also have been in touch with the StarlingX
build system and many of the Platform Services. Was also involved on the last
three StarlingX releases, from feature planning to testing and delivery.
I guess mine can be a new pair of hands to help continuously enhance the system,
grow the community and reduce the barriers to user adoption.

Thank you for your consideration.

Thales Elero Cervi
