StarlingX continues to be an exciting and growing project to which I have
been lucky enough to contribute to from its launch in early 2018.  Over
the past 4 1/2 years I have provided technical guidance to several of
StarlingX's subproject teams, represented StarlingX in OpenStack's Edge
Computing Group, and participated directly in the StarlingX Documentation
subproject.

As PL of the 'docs' project, I am interested in:
- continuing to provide guidance and technical reviews on managing
  new doc content in docs.starlingx.io
- continuing to provide leadership and guidance on enhancements
  and re-structuing of docs.starlingx.io
- making docs.starlingx.io one of the core strengths of StarlingX.
