Candidate for StarlingX Flock Services Technical Lead Role

I have contributed to the StarlingX project from inception and developed
seed code for StarlingX.  In particular, I have developed the
architecture, specifications and code in several key areas, including
config, distributed cloud, and monitor.  I participate in flock services
community calls and also serve as core reviewer in several StarlingX projects
where I strive to provide constructive comments for features and quality.

As technical lead for flock-services, I would focus on:
* Ensuring high quality by providing guidance on specs and feedback
  on reviews, and encouraging improvements
* Growing contributors to flock-services project by enabling ease
  of contribution and soliciting developers and users feedback
* Evolving the flock-services features to ensure its value in the
  integration of StarlingX services for cloud-native applications

Thank you for your consideration for my candidacy as the technical lead for
the StarlingX flock-services project,

John Kung
