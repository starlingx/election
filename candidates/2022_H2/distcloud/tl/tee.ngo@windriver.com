Candidate for StarlingX Distributed Cloud Technical Lead Role

I have contributed to the StarlingX project from the inception.  In particular, I have
developed the architecture updates, feature specifications and code in several key
areas including ansible playbooks, config and distributed cloud.  I developed the
seed code for the ansible playbooks repo and the application framework of config repo.
In recent releases of StarlingX, I have been driving the delivery of high impact
distributed cloud capabilities such as scalability improvements (500% increase),
increase in parallel operations, subcloud prestaging. I currently am driving the
transition of distributed cloud from CentOS to Debian and the delivery of new
capabilities. In addition, I've provided my input on new capabilities in other StarlingX
projects to ensure their successful integration and operations in distributed cloud
environment.

I have been performing my duties as a core reviewers in several StarlingX projects where I
aim to provide assistive comments for features, repo quality and to foster knowledge
sharing.

As technical lead for distributed cloud, I would focus on:
* Ensuring high quality by providing guidance on specs and feedback
  on reviews, and encouraging improvements
* Growing contributors to distributed cloud project by enabling ease
  of contribution and soliciting developers and users feedback
* Evolving the distributed cloud features so that its scalability,
  versatility and ease of operations will enable many more use cases thus
  increasing StarlingX adoption.

Thank you for your consideration for my candidacy as the technical lead for
the StarlingX distributed cloud project.

Tee Ngo
