My name is Thales Elero Cervi and I am nominating myself for election
to the StarlingX Openstack-Distro PL role.

I'm working with stx-openstack application for about one year and half
and recently started to represent the Openstack-distro project on our PTG,
our weekly TSC/Community calls and our Planning calls.
I'm also leading the development and test efforts for delivering the planned
features since release stx/7.0.

My goal for the project is to ensure the stx-openstack application is stable,
reliable and compatible with the latest versions of StarlingX, including the
migrations for FluxCD and Debian. I also care for the testing activities to
ensure software quality and that upgrade processes are covered properly.
