Edge computing is an unique area with interesting problems
to solve.  It also has tremendous opportunities.  StarlingX
project continues to solve many challenges with numerous
exciting features and capabilities.  I am happy to
be part of this community.  Over the past few
months, I have immensely enjoyed contributing to the config,
flock services and distributed cloud sub-projects as a PL.

As part of TSC, I am excited about the following:
- Support and shape the evolution of StarlingX
- Support and grow the StarlingX users and contributors

Looking forward to working with the community on this
journey.