Hello, My name is Scott Kamp, I am nominating myself for election to StarlingX TSC.

I am an avid user and advocate of the StarlingX community over the past years.

My goals for the StarlingX commmunity is to bring StarlingX to a larger global user base.
I am intent on growing the community and helping StarlingX become a more community
freindly solution for users to realize its potential allowing for a more glabal
footprint and adoption into many industries including IOT, Smart Home, and Telecomm.
I intend to also enable the community to create reproducible builds and provide a
upgrade path to current deployments moving forward, while also contributing features
that I feel are required. this will allow more more users to become involved, an more
feedback will come to the community via the extended user base as it grows which makes
StarlingX a more versitile, stable, scalable and more secure system.

Thank you for considering me as a contributor to the TSC.
