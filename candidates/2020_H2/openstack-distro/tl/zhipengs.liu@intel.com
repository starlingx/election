Zhipeng Liu: Candidate for StarlingX Openstack Technical Lead role
Affiliation: Intel
I have been contributing to the StarlingX project since StarlingX Day one.

As the technical lead for the Openstack project I would focus on:
* Follow up and integrate upstream openstack new feature and maintain
  StarlingX openstack config, fix bugs.

* Responsible for openstack related change review, such as openstack helm-chart
openstack config.

* Reinforcing interaction between Openstack and other StarlingX projects by Studying
  StarlingX new feature, design and requirement.

Thank you

Zhipeng Liu
