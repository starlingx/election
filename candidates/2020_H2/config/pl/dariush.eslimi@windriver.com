My name is Dariush Eslimi and I am nominating myself for StarlingX distributed 
cloud project lead.

I have been an active member of StarlingX community and current project lead 
for config.

My main goal for the project is wide adoption of this project in every possible
industry that it can make a difference. I am passionate about solving 
real world problems and seeing StarlingX used by as many people as possible.
I am proud of what we have achieved so far and would like to help push us even 
further.

Thanks,
Dariush
