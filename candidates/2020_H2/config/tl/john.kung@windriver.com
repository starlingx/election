Adding John Kung candidacy for project config TL

I have contributed to the seed code for the StarlingX config project from
inception and have contributed to the design and code in the config service.
As core reviewer for config, and related projects such as stx-puppet,
ansible-playbooks, metal, distcloud, I have provided guidance during specs and
code reviews for architectural integration of these projects.

As the technical lead for the config project, I would focus on:
* Maintaining the high level of quality in the config project.  I will do this
  by reviewing and providing feedback to feature specifications in this
  project. I will continue my role as a core reviewer in this project and
  related projects which affect or are dependent on this project.
* Grow contributors to config by providing guidance during the specification
  and implementation phases.
* Improve config for better maintainability while improving scalability and
  enabling features to support a diverse config capabilities. This will
  increase the value of config services by providing a controlled and
  maintainable config environment.

Thank you for your consideration for me to serve as the technical lead for the
StarlingX config project.

John Kung
