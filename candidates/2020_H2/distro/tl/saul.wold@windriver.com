Saul Wold: Candidate for StarlingX "Distro" Technical Lead
Affiliation: Windriver

As the current Technical Lead (TL) for the Non-OpenStack Distro or core-OS
 sub-project, I would like to continue working for the community to reduce
the existing technical debt (in the form of patches). The work to upstream
the existing changes would benefit both the StarlingX community and the
upstream project.

I bring a background of development in Open Source projects and OS build
tools and working with many upstream open source projects via my past 
work with the Yocto Project and OpenEmbedded communities.

This sub-project is going to be very important for the StarlingX-5.0
release as we continue to work on getting the CentOS base OS updated to
Centos-8.

I appreciate your time and consideration of me to continue serving on
the StarlingX Project Distro team as Technical Lead.

Thanks
   Sau!

Saul Wold
