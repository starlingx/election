Steven Webster: Candidate for StarlingX Networking Project Lead role
Affiliation: Wind River Systems

As the current PL for the StarlingX networking sub-project I nominate
myself to continue serving as the PL.

I have worked extensively in the networking domain since the inception of
StarlingX and take an active role in triaging bug reports, monitoring the
StarlingX mailing list, and chairing the bi-weekly networking meeting.

As PL for the project I will continue to coordinate support, features,
and other activities in the networking domain.

Thanks for your consideration

Steven Webster
