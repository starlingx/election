As the current PL for the StarlingX Containers sub-project I nominate myself
to continue serving as the PL.

I am one of the founding members of StarlingX and worked with a large
number of very bright community members to introduce containerized services
to the project.

As PL for the project I will continue to work with the TL and project
members to complete the decoupling of applications from the platform as
well as support initiatives to further introduce amd/or convert services
to run inside containers.

Thanks for your consideration.

Frank Miller

