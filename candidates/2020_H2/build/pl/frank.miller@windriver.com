As the current PL for the StarlingX build sub-project I nominate myself
to continue serving as the PL.

I am one of the founding members of StarlingX and have worked closely
with the build team to maintain and support the build infrastructure
for the project.

As PL for the project I will continue to coordinate support and other
activities in the build domain.  

Thanks for your consideration.

Frank Miller

