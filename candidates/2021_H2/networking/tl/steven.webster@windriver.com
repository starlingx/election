Steven Webster: Candidate for StarlingX Networking Technical Lead role
Affiliation: Wind River Systems

I have worked extensively in the networking domain since the inception of
StarlingX.  As the current PL for the networking sub-domain, I take an
active role in triaging bug reports, monitoring the StarlingX mailing list,
chairing the bi-weekly networking meeting, and participating in networking
related contributions.

As the technical lead for the networking project, I would focus on a few things:
* Present a renewed focus on improving the starlingX networking documentation 
* Maintaining the high level of quality in the networking domain, with
  constructive and supportive guidance to existing contributors.
* Place an emphasis on the encouragement and guidance of new contributers to
  the networking space.
* Driving the bug-backlog down, especially around the important but
  non-critical defects.

Thanks for your consideration of the StarlingX Networking Technical Lead role.

Steven Webster
