Chris Friesen: Candidate for StarlingX Containers Technical Lead role
Affiliation: Wind River Systems

As a founding member, ongoing contributor, and core reviewer for multiple
StarlingX repositories I would like to submit my candidacy as TL for the
Containers project.

I have over seven years of experience with the original Wind River codebase
and derived StarlingX project code.  I have reviewed and contributed code
across multiple areas of the project and have a good understanding of
the interrelationships between different components.

Historically my focus has been on virtualization and how it was managed
by OpenStack services, especially nova.  More recently as StarlingX has
become primarily a Kubernetes platform I have worked on the various
containerization components, and in the past year I have been leading the
effort to support upgrading Kubernetes from 1.18 to 1.21 in one release.

As the TL for the containers project, I will work closely with the PL to
help influence the implementation of new TSC approved features and
improvements to existing functionality.  I would like to work with the
communtiy to achieve these goals.

Thanks for your consideration.

Chris Friesen
