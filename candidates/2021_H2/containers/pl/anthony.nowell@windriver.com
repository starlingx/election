I joined the StarlingX community in June of this year. I have worked closely with Frank Miller, the
current Containers PL, and assumed responsibility for the container infrastructure team and projects
related to StarlingX within Wind River such as the current effort to support Kubernetes 1.21 in
StarlingX 6.0.

Prior to joining the StarlingX community, I spent several years in both technical and team
leadership roles supporting infrastructure (Linux, multi-cloud, Kubernetes) for an enterprise ML
platform. I have been working with containers since the original Docker release in 2013, and
Kubernetes since version 1.5 in 2016.

As PL for the project, I will establish a cadence to discuss trends and advances in the containers
and CNCF ecosystems to foster brainstorming and discussion around implications and opportunities for
StarlingX.

Thanks for your consideration.

Anthony Nowell
