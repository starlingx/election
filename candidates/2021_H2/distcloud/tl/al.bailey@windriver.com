Candidate for StarlingX DistCloud Technical Lead Role

I have contributed to the StarlingX project from inception and developed
the seed code for StarlingX.  
I have worked in nearly all areas of StarlingX , including distributed cloud,
nfv, fault, config, etc..
 I also serve as core reviewer on many StarlingX projects.
comments for features and quality.

As technical lead for distcloud, I would focus on:
* Ensuring high quality by providing guidance on specs and feedback
  on reviews, and encouraging improvements
* Growing contributors to distcloud project by enabling ease
  of contribution and soliciting developers and users feedback
* Evolving and simplifying the distcloud codebase

Thank you for your consideration for my candidacy as the technical lead for
the StarlingX distcloud project,

Al Bailey
