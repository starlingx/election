Mark Asselstine Candidate for StarlingX Distro Technical Lead role
Affiliation: Wind River Systems

I have been committed to the transition away from CentOS as the
provisional PL for the StarlingX Distro working group and would like
to continue to see this transition through. The comming months and
year will see lots of excitement as we approach the first release
based on Debian and I hope to play a continued role in this work.

Thanks,
Mark
	      
