When I joined Wind River in June of 2021 to lead its Cloud Platform
Validation and Verification test team, I was introduced to the StarlingX
project. I’m new to opensource projects and am intrigued by their nature.

For the majority of my career I’ve been involved in leading and building
test teams across multiple different software markets.  I have a number
of years’ experience leading technical test teams and helping them set
direction and solving difficult test challenges.  I’m eager to use my
experience leading these teams to influence the direction of StarlingX
testing as the Test PL/TL.  I will endeavour to learn the current test
process for StarlingX and look for ways to enhance and grow this
important element of the project.

Thanks for the consideration as the new StarlingX Test PL/TL.

Regards,

Rob Cooke
