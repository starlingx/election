I joined the StarlingX community in June of this year, and I have been a consistent participant in
the StarlingX Build sub-project since. I have worked closely with Frank Miller, the current Build
PL, and have assumed responsibility for the build team and projects related to StarlingX within Wind
River such as the current effort to implement build support for the larger Debian effort.

Prior to joining the StarlingX community, I spent several years in both technical and people
leadership roles supporting infrastructure (Linux, multi-cloud, Kubernetes) for an enterprise ML
platform.

As PL for the project, I will first ensure continuity of existing established build initiatives and
processes, and subsequently look to improve them with the support of the TL and project members. As
the number of contributors to StarlingX increases, I'll be looking to improve visibility into build
trends (i.e. success rate & duration) to guide investments, and I intend to advocate for efforts
that containerize the entire build system.

Thanks for your consideration.

Anthony Nowell
