My name is Wang Hao and I am nominating myself for election to the StarlingX
Technical Steering Committee. I have been a contributor in OpenStack since
2013 and StarlingX since 2019.

Edge computing is developing rapidly all over the world with the 5G.
StarlingX is very important open source project in the Edge area
and it has been adopted into so many different scenarios and getting more
and more maturer from 1.0 to 4.0.

Its very honored to me that I was being the one of TSC members in 2019 H1.
In this year, I worked together with other members to focus on improving the
development of StarlignX in China market. We held some great meetings to
introduce StarlingX like meet-up of StarlingX 2.0 and hackathons of OpenInfra,etc.
Also we have communications with the developers and customers to gather the
requirements and feedbacks to StarlingX community.

In 2020, there're some critical verifications for StarlingX in China and I think
that may bring some new challenges like security enhancement or multi-arch support.
My colleagues and I will keep improving this community forward as we used to do.

It's a fantastic experience working with this team and I would be
pleased to serve as TSC to push on StarlingX better in 2020.

Thank you all
