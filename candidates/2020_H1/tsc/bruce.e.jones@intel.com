Hi.  I am Bruce Jones and I am running for a position on the StarlingX TSC.

I have been a member of the StarlingX community since the beginning of the
project, before it was called StarlingX.  I led the work to create
the project and release the "seed code" to the community.  I have since served as 
a PL for the distro.openstack project and am currently a Core Reviewer on the
documentation team.

My goals for the project are to build on the strengths that we collectively
have created while growing our community of users and developers.  

To acheieve those goals I will help plan and lead efforts around building
features that our users need, while addressing the issues they raise as the
evaluate and deploy the software.  I will listen to the feedback of the 
community and work to resolve the problems important to us as the developers
and maintainers of this software.  And I will always act to support the 
Four Opens and to ensure the long term success of the project.
