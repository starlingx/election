StarlingX is an exciting and growing project to which I have had the honor 
and privilege to contribute to from its launch.   I am a founding member of 
the TSC and over the past 2 years I have helped facilitate the TSC, welcome 
new members and help guide the project from a technical perspective.  I am a 
frequent speaker at industry events on the values of StarlingX and share my 
passion for edge computing widely.  I see edge as having the ability to 
transform people's lives through transformation of technology used in 
communications networks, health care and transportation - StarlingX is a big 
part of making that happen.
 
I have a deep technical knowledge of distributed computing and optimization of 
edge focused solutions. I have helped out in the Edge Computing talk selection 
committee for the Shanghai summit.
 
I was honored and humbled to receive a community award at the Denver summit, 
for my contributions to the StarlingX project - [0]

On the TSC I would continue to have the following areas of focus:

- Continue to advocate for the 4 Opens model of development. 
- My goal is to champion and contribute to increasing test focus a and culture 
   of the project around test.
   - This area is so important to the health of any open source project. 
   - This will help new developers contribute more easily and the project to 
     grow the contribution base and speed of development. 
   - We had a test focused Hack-a-thon and plan on working to set up another 
     one later this year.
 
Growing the community, maintaining the technical relevance of the project and
continuing StarlingX's journey to a diverse and thriving open source project 
are goals I want to contribute to for StarlingX.
 
Thank you for your consideration of me as a candidate to be re-elected to the 
TSC for a second term.
 
[0] https://superuser.openstack.org/articles/open-infrastructure-community-contributor-awards-denver-summit-edition/