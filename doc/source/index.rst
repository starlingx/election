==================
StarlingX Election
==================

.. toctree::
   :hidden:

   process

StarlingX PL, TL and TSC Elections Timeline
===========================================

+-------------------+------------------------------+------------------------------+
| Events            | Start Date                   | End Date                     |
+===================+==============================+==============================+
| TSC Nominations   | October 01, 2024, 20:45 UTC  | October 08, 2024, 20:45 UTC  |
+-------------------+------------------------------+------------------------------+
| TSC Campaign      | October 08, 2024, 20:45 UTC  | October 15, 2024, 20:45 UTC  |
+-------------------+------------------------------+------------------------------+
| TSC Election      | October 15, 2024, 20:45 UTC  | October 22, 2024, 20:45 UTC  |
+-------------------+------------------------------+------------------------------+
| PL/TL Nominations | October 01, 2024, 20:45 UTC  | October 08, 2024, 20:45 UTC  |
+-------------------+------------------------------+------------------------------+
| PL/TL Election    | October 15, 2024, 20:45 UTC  | October 22, 2024, 20:45 UTC  |
+-------------------+------------------------------+------------------------------+

See `Election system`_, `PLTL details`_ and `TSC details`_.


Election Officials
==================

* Ildiko Vancsa (ildikov), ildiko at openstack dot org
* Abhishek Jaiswal (ajaiswal), Abhishek.Jaiswal at windriver dot com

For any questions, please contact officials by mail or on Matrix in the
StarlingX-General room in the [#starlingx:opendev.org](https://matrix.to/#/#starlingx:opendev.org)
space.


.. _Election system:

Election System
===============

Elections will be held using CIVS and a Condorcet algorithm
(Schulze/Beatpath/CSSD variant). Any tie will be broken using
`Governance_TieBreaking`_.

Election Process
================
The election process is outlined on the :doc:`process` page.


Electorate
==========

The electorate for this election are individuals who earned the contributor
status some time in the past 12 months prior to the nomination period starts as
defined by the `TSC charter`_ document.

The electorate is requested to confirm their email address in gerrit,
review.opendev.org > Settings > Contact Information > Preferred Email,
prior to September 27, 2024 so that the emailed ballots are mailed to the correct
email address.

The electorate is expected to abide by the following general resolution:
https://docs.starlingx.io/governance/resolutions/20190520_election_activities.html

Candidates
==========

Any member of an election electorate can propose their candidacy for the same
election. Nominees propose their candidacy by submitting a text file to the
starlingx/election repository. See documentation below.


.. _How to submit a candidacy:

How to submit a candidacy
=========================

Each candidate must nominate themselves for each elected position, and are
encouraged to submit their own candidacy to gerrit, although where
appropriate, others may submit a candidacy for those who have already
self-nominated by other means.

If you are not already familiar with StarlingX development workflow, see this
more detailed documentation:
https://docs.starlingx.io/contributor/index.html

Candidacies now need to be submitted as a text file to the starlingx/election
repository. Here are the required steps:

* Clone the election repository:
  ``git clone https://opendev.org/starlingx/election.git ; cd election``
* Create a new file
  candidates/<election_cycle>/<project_name>/<leadership_role>/<email_address>
  containing the candidate statement.
* Commit the candidacy:
  ``git add candidates/<election_cycle>/<project_name>/<leadership_role>/<email_address>;
  git commit``
* In the text editor add a title like the following:

  Adding <your_name> candidacy for <project_name> <project_role> role

* Save the text and exit the text editor
* Submit your candidacy: git review

For example Dana Developer (ddev on IRC) would compose a platform in a file
named "candidates/2020_H2/fault/pl/dana\@inconnu.org" to submit a PL candidacy
for the Fault Management project elections.

In case a candidate has difficulties with submitting their nomination to Gerrit
they may ask a fellow contributor to upload the text file on behalf of them.

After the candidacy is submitted to gerrit, verification and approval will
be performed by elections officials, followed by an update to the approved
candidate list.


.. _TSC details:

Technical Steering Committee's election
=======================================

Elected Positions
-----------------

Under the rules of the `TSC charter`_, we need to renew 2 TSC seats for this
election, and fill a currently empty seat. Seats are valid for one-year terms.

* Technical Steering Committee member - 3 positions

Electorate
----------

The electorate for this election are individuals who earned the contributor
status some time in the past 12 months prior to the nomination period starts as
defined by the `TSC charter`_ document.


Candidates
----------

Any contributor who is an individual member of the foundation can propose their
candidacy.

Nominees propose their candidacy by submitting a text file to the
starlingx/election repository. The file must be placed in
candidates/<electcycle>/TSC/<email_address>.
The candidacy is then confirmed by elections officials through gerrit.
See above `How to submit a candidacy`_ documentation.


.. _PLTL details:

Project Leads' and Technical Leads' election
============================================

Elected Positions
-----------------

Every `official project team`__ must elect a `PL`_ and a `TL`_. PLs and TLs are
elected for a one year period.

__ `official project teams`_


Electorate
----------

Except otherwise-noted in the project team description, the electorate for a
given PL and TL election are the individuals that are also committers for one
of the team's repositories over the past 12 months prior to the nomination
period starts as defined by the `TSC charter`_ document.


Candidates
----------

Any contributor who is an individual member of the foundation can propose their
candidacy to run for PL.

Any contributor who is an individual member of the foundation and a `core
reviewer`_ to the given project can propose their candidacy to run for TL of
the given project.

Nominees propose their candidacy by submitting a text file to the
starlingx/election repository. The file must be placed in
candidates/<electcycle>/<project_name>/<leadership_role>/<email_address>.
The candidacy is then confirmed by elections officials through gerrit.
See above `How to submit a candidacy`_ documentation.


.. seealso::

  See the `Election Officiating Guidelines`_ page in the wiki for details on the
  election process.

.. _Election Officiating Guidelines: https://wiki.openstack.org/wiki/Election_Officiating_Guidelines
.. _Governance_TieBreaking: https://wiki.openstack.org/wiki/Governance/TieBreaking
.. _TSC charter: https://docs.starlingx.io/governance/reference/tsc/stx_charter.html
.. _official project teams: https://docs.starlingx.io/governance/reference/tsc/projects/index.html
.. _PL: https://docs.starlingx.io/governance/reference/tsc/stx_charter.html#project-lead
.. _TL: https://docs.starlingx.io/governance/reference/tsc/stx_charter.html#technical-lead
.. _core reviewer: https://docs.starlingx.io/governance/reference/tsc/stx_charter.html#core-reviewer
