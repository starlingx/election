.. _election-process:

================
Election Process
================

The election process is administered and guided by the Election Officials.

This group is responsible for working with the TSC on the election timeline,
performing the preparation steps and carrying out all required communications.

Pick election officials
=======================

Election officials should not run for election themselves. Ideally they should
also have no interest in the election outcome (to preserve neutrality) but that
is generally harder to achieve.

TSC Elections
=============

Selecting Election Dates
------------------------

Things to keep in mind when selecting election dates:

* Try to void overlapping with big in-person events in the interest for the
  community or major public holidays
* Allow time for the contributors to update their primary email address in Git
  if needed and update the 'extra-atcs' list in the 'projects.yaml' file
* Allow at least a week for nomination and campaign periods
* The current TSC needs to approve the timeline once there is a proposal

Preparation
-----------

As early as possible but at least a month before election starts:

* Edit elections details (timeline, elected positions, deadlines):
  * Create the commit to update website

A couple of weeks before election starts:

* Send *Upcoming TSC elections* email
* Generate the electorate list and share it with the community to ensure that
  the list contains all active contributors
* Make sure that the 'extra-atcs' fields in the `projects.yaml`_ file in the
  governance repository are up to date.

extra-atcs
~~~~~~~~~~

The 'extra-atcs' field serves the purpose to capture people who are actively
participating in the community, but don't have a contribution to any of the git
repos that the project teams maintain.

The information is stored with an expiration date of 12 months, since the
community defines `active contributors`_ with that timeframe.

TSC Candidacy Round
-------------------

When TC Candidacy starts:

* Send *StarlingX TSC election - Nomination period started* email

During the TC Candidacy round Election officials review the nominations in
Gerrit:

* To +2 a candidate:

  * check candidate profile using https://www.openstack.org/community/members/
  * check filename is email address
  * cursory check the candidacy statement

* To +Workflow, check the previous +2 details

A couple of days before the candidacy submission ends:

* Send *StarlingX TSC election - Nomination period started* reminder email

When TC Candidacy submission ends:

* Send *StarlingX TSC election - Nomination period ended* email

Once the email deadline is reached:

* Check if there are enough candidate to run the election

  * If yes

    * Generate the electorate rolls.
    * Move forward with the next steps of the process

  * If not

    * Reach out to the TSC and have the active members (whose seats are not up
      for re-election) officiate the results before the last steps of
      administration


TSC Campaigning
---------------

The TSC election includes a period after the candidates are defined but before
the election, for candidates to answer questions from the community.  Open this
with *StarlingX TSC election - Campaign period started* email.


TSC Election Round
------------------

Before TSC Election begins:

* Create CIVS page

  * Title the poll: <Year> <H1/H2> Technical Steering Committee Election Poll
  * Enable detailed ballot reporting
  * Send to other officials to verify

    * Check number of seats
    * Check closing date

When TSC Election begins:

* Upload rolls

  * CIVS has a maximum number of electorate emails you can upload at a time
    without crashing, limit to 500 at a time
  * Send *StarlingX TSC election - Voting period started* email

A couple of days before the TSC Election ends:

* Send *StarlingX TSC election - Voting period started* reminder email

Ending TSC Election
-------------------

* Close the election and send *StarlingX TSC election - Voting period ended*
  email
* Update the list of TSC members and their terms in the governance repository

  * Commit change and push review

* Send *TSC Election Results* email


PL/TL Elections
===============

Selecting Election Dates
------------------------

The PL/TL elections are running in parallel to the H2 TSC election using the
same timeline.

The preparation steps and process are the same as for the TSC elections. The
emails and communication should contain the information about the PL/TL
elections during the H2 period.

Ending PL/TL Election
---------------------

Once the PL/TL election results are ready the 'projects.yaml' file needs to be
updated with the elected PLs and TLs.

.. _projects.yaml: https://opendev.org/starlingx/governance/src/branch/master/reference/tsc/projects.yaml
.. _active contributors: https://docs.starlingx.io/governance/reference/tsc/stx_charter.html#contributors
